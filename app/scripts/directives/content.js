'use strict';
google.load("feeds", "1");

/**
 * @ngdoc directive
 * @name wallarssApp.directive:content
 * @description
 * # content
 */
angular.module('wallarssApp')

.controller('rssFeederCtrl', function ($scope,$http,rssFeed) {

    $scope.rssfeedsObj = {};
    $scope.rssFeedArr = [];
    
  // -- ordering the information in fit variables  
    $scope.initRssFeedArray = function(){
      if ($scope.selectedObj) {
          //-- using the rssFeed service to bring the url's data like a boss
        rssFeed.get($scope.selectedObj).then(function(result) {
          if (result.error) {
            console.error("ERROR " + result.error.code + ": " + result.error.message + "\nurl: " + url);
            $scope.setLoading(false);
          }
          else {
            $scope.rssfeedsObj = result.feed.entries;
            $scope.rssFeedUrl = result.feed.feedUrl;
          }
        });

      }
    };

    $scope.getFeedUrl = function(){
      return $scope.rssFeedUrl;
    };

    // --watching on selectedObj - that way I know when
    // --different url is chosen.
    $scope.$watch('selectedObj', function () {
      $scope.initRssFeedArray();
    });

  
})

  .directive('content', function () {
    return {
      templateUrl: 'scripts/directives/content.html',
      restrict: 'E',
      controller: 'rssFeederCtrl',

    };
  });
