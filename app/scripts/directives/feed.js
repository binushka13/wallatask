'use strict';

/**
 * @ngdoc directive
 * @name wallarssApp.directive:feed
 * @description
 * # feed
 */
angular.module('wallarssApp')

  .controller('FeedCtrl', function ($scope) { 
    
    $scope.title = $scope.feedinfo.title;
    $scope.date = $scope.feedinfo.publishedDate; 
    
    //-- getter for the content of the feed --
    $scope.getContent = function (){
      
      return $scope.feedinfo.content;
    };

  })

  .directive('feed', function () {
    return {
      templateUrl: 'scripts/directives/feed.html',
      restrict: 'E',
      controller: 'FeedCtrl',
      scope: {
        feedinfo: '=?'
      }
    };
  });
