'use strict';

/**
 * @ngdoc service
 * @name wallarssApp.rssFeed
 * @description
 * # rssFeed
 * Service in the wallarssApp.
 */
angular.module('wallarssApp')
  .service('rssFeed', function ($q,$rootScope) {

    // --the .get will do all the hard work!
    this.get = function(url) {
      var d = $q.defer();
        
     // -- that way, i'll have the information logicly inside "feed". Thanks google !
      var feed = new google.feeds.Feed(url);
        
    // -- I limited the number of entries with 50! 
      feed.setNumEntries(50);
        
      feed.load(function(result) {
        $rootScope.$apply(d.resolve(result));
      });
      return d.promise;
    };
  });
