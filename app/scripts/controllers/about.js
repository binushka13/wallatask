'use strict';

/**
 * @ngdoc function
 * @name wallarssApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the wallarssApp
 */
angular.module('wallarssApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
