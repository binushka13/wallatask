'use strict';

/**
 * @ngdoc function
 * @name wallarssApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the wallarssApp
 */
angular.module('wallarssApp')
  .controller('MainCtrl', function ($scope) {
    
    $scope.input = 0;
    
    // if the urls array in localstorage was empty - restart it, otherwise
    //take the last version of ot from localstorgae
      if (localStorage.getItem("urls")) {
          $scope.urls = JSON.parse(localStorage.urls);
      } else {
          $scope.urls = [1];
      }
     
    //--- when user inserts a new url, we'll push it to the urls array
    $scope.submit = function (input) {
		if (input) {
			$scope.urls.push(input);
            //make sure it's updated in localstorage
            $scope.updateLocalStorage();
		}
	 };
    
    //----when user clicks on "X"- we'll delete the specific url by
    //----splicing the urls array
     $scope.delete = function(url) {
          var index = $scope.urls.indexOf(url);
          if (index) {
               $scope.urls.splice(index, 1);
              //--- make sure the localstorage is up to date---
               $scope.updateLocalStorage();
          }
      };
      
    //-- when specific url is being clicked, update the fit fields -- 
    // -- that way the $watch in the content directive will start --
      $scope.select = function (index) {
          $scope.selectedObj = $scope.urls[index];
          $scope.selectedIndex = index;
      };

    // -- updating the local storage with the current urls array
      $scope.updateLocalStorage = function () {
        localStorage.setItem("urls", JSON.stringify($scope.urls));  
      };
  });
