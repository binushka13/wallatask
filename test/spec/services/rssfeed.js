'use strict';

describe('Service: rssFeed', function () {

  // load the service's module
  beforeEach(module('wallarssApp'));

  // instantiate service
  var rssFeed;
  beforeEach(inject(function (_rssFeed_) {
    rssFeed = _rssFeed_;
  }));

  it('should do something', function () {
    expect(!!rssFeed).toBe(true);
  });

});
